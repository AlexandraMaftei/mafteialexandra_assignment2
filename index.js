
const FIRST_NAME = "Alexandra";
const LAST_NAME = "Maftei";
const GRUPA = "1092";

/**
 * Make the implementation here
 */

function initCaching() {
   var cache = {};
   var obj = {};
   cache.pageAccessCounter = function(param)
   {
        if(param==null && obj.hasOwnProperty('home'))
            obj['home']++;
        else if (param==null && obj.hasOwnProperty('home')==false)
            obj['home']=1;
        else {
            var page = String(param).toLowerCase();
            if(obj.hasOwnProperty(page))
                obj[page]++;
            else obj[page]=1;
        }
   }

   cache.getCache = function()
   {
        return obj;
   }

   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

